//
//  PageThree.swift
//  CustomUINav
//
//  Created by Curtis Turk on 16/10/2023.
//

import Foundation
import SwiftUI

struct PageThree: View {
    
    @EnvironmentObject private var coordinator: Coordinator
    
    var body: some View {
        ScrollView{
            VStack(spacing: 50){
                Text("Third Page time")
                    .padding()
                Image(systemName: "lightbulb")
                    .font(.largeTitle)
                Button("Page Three Detail"){
                    coordinator.push(.pageThreeDetail)
                }
                .foregroundStyle(.blue)
            }
        }
        .navigationTitle("Page Three")
    }
}

#Preview {
    PageThree()
}
