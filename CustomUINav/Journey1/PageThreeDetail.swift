//
//  PageThreeDetail.swift
//  CustomUINav
//
//  Created by Curtis Turk on 17/10/2023.
//

import SwiftUI

struct PageThreeDetail: View {
    
    @EnvironmentObject private var coordinator: Coordinator

    @State var text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id fringilla magna, ut tempus lacus. Aenean eget feugiat ex. Curabitur dapibus aliquet fermentum. Nulla rhoncus risus id enim posuere, quis dapibus purus pellentesque. Vivamus eget arcu consequat, laoreet arcu ac, elementum ligula. Donec ac aliquet urna, sit amet varius nunc. Curabitur eu erat dictum, facilisis sapien nec, convallis erat. Maecenas consectetur elit eget purus congue malesuada. Curabitur aliquet leo egestas ante viverra, vitae fermentum quam eleifend. Donec porttitor sodales nisl, vitae eleifend dui scelerisque sit amet. Vivamus vestibulum turpis vel lacus tincidunt dignissim. Phasellus ut mi et ante tristique pellentesque quis sed leo. Fusce vehicula viverra tellus. Duis scelerisque felis sit amet risus lobortis, vel lobortis ante lobortis. Donec auctor, neque auctor vulputate gravida, nisi tortor ultrices sapien, sit amet laoreet tortor risus quis dolor."
    
    @State private var searchText = ""
    
    var searchResults: String {
        if searchText.isEmpty{
            return text
        } else {
            if text.contains(searchText) {
                return "True"
            }else {
                return "False"
            }
        }
    }
    
    var body: some View {
        ScrollView{
            Text(text)
                .padding()
        }
        .searchable(text: $searchText, prompt: "Lorem")
        .navigationTitle("Journey 1")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            Button(){
                coordinator.popToRoot()
            } label: {
                Image(systemName: "xmark")
            }
        }
    }
}

#Preview {
    PageThreeDetail()
}
