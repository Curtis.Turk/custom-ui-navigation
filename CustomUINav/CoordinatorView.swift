//
//  CoordinatorView.swift
//  CustomUINav
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

struct CoordinatorView: View {
    
    @StateObject private var coordinator = Coordinator()
    
    var body: some View {
        NavigationStack(
            path: $coordinator.path) {
                coordinator.build(page: .pageOne)
                    .navigationDestination(for: Page.self) { page in
                        coordinator.build(page: page)
                    }
            }
            .navigationAppearance(
                backgroundColor: .orange,
                foregroundColor: .white,
                tintColor: .white
            )
            .environmentObject(coordinator)
    }
}

#Preview {
    CoordinatorView()
}
