//
//  CustomUINavApp.swift
//  CustomUINav
//
//  Created by Curtis Turk on 16/10/2023.
//

import SwiftUI

@main
struct CustomUINavApp: App {
    var body: some Scene {
        WindowGroup {
            CoordinatorView()
        }
    }
}
