//
//  Coordinator.swift
//  CustomUINav
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

enum Page: String, Identifiable {
    case pageOne, pageTwo, pageThree, pageThreeDetail, pageFour, pageFourDetail
    
    var id: String {
        self.rawValue
    }
}

class Coordinator: ObservableObject {
    @Published var path = NavigationPath()
    
    func push(_ page: Page){
        path.append(page)
    }
    
    func pop(){
        path.removeLast()
    }
    
    func popToRoot(){
        path.removeLast(path.count)
    }
    
    @ViewBuilder
    func build(page: Page) -> some View {
        switch page {
        case .pageOne:
            PageOne()
        case .pageTwo:
            PageTwo()
        case .pageThree:
            PageThree()
        case .pageThreeDetail:
            PageThreeDetail()
        case .pageFour:
            PageFour()
        case .pageFourDetail:
            PageFourDetail()
        }
    }
}
