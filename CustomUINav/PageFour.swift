//
//  PageFour.swift
//  CustomUINav
//
//  Created by Curtis Turk on 16/10/2023.
//

import Foundation
import SwiftUI

struct PageFour: View {
    
    @EnvironmentObject private var coordinator: Coordinator
    
    var body: some View {
        ScrollView{
            VStack(spacing: 50){
                Text("Is this the final page?")
                    .padding()
                Image(systemName: "snowflake")
                    .font(.largeTitle)
                Button("Page Four Detail"){
                    coordinator.push(.pageFourDetail)
                }
                .foregroundStyle(.blue)
            }
        }
        .navigationTitle("Page Four")
    }
}

#Preview {
    PageFour()
}
