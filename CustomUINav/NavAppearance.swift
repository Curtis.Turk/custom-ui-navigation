//
//  NavAppearance.swift
//  CustomUINav
//
//  Created by Curtis Turk on 19/10/2023.
//

import Foundation
import SwiftUI

struct NavAppearanceModifier: ViewModifier {
    
    init(
        backgroundColor: UIColor,
        foregroundColor: UIColor,
        tintColor: UIColor?,
        hideSeparator: Bool
    ){
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.backgroundColor = backgroundColor
        navBarAppearance.titleTextAttributes = [.foregroundColor: foregroundColor]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: foregroundColor]
        if hideSeparator { navBarAppearance.shadowColor = .clear }
        
        UINavigationBar.appearance().tintColor = tintColor
        UINavigationBar.appearance().standardAppearance = navBarAppearance
        UINavigationBar.appearance().compactAppearance = navBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
    }
    
    func body(content: Content) -> some View {
        content
    }
}

extension View {
    func navigationAppearance(
        backgroundColor: UIColor,
        foregroundColor: UIColor,
        tintColor: UIColor? = nil,
        hideSeparator: Bool = false
    ) -> some View {
        self.modifier(
            NavAppearanceModifier(
                backgroundColor: backgroundColor,
                foregroundColor: foregroundColor,
                tintColor: tintColor,
                hideSeparator: hideSeparator
            )
        )
    }
}
