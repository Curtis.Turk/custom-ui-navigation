//
//  PageFourDetail.swift
//  CustomUINav
//
//  Created by Curtis Turk on 17/10/2023.
//

import SwiftUI

struct PageFourDetail: View {
    
    @EnvironmentObject private var coordinator: Coordinator
    
    var body: some View {
        ScrollView{
            Text("Last page!")
                .padding()
        }
        .navigationTitle("Journey 2")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            Button(){
                coordinator.popToRoot()
            } label: {
                Image(systemName: "xmark")
            }
        }
    }
}

#Preview {
    PageFourDetail()
}
