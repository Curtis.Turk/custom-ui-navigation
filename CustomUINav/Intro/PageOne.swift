//
//  ContentView.swift
//  CustomUINav
//
//  Created by Curtis Turk on 16/10/2023.
//
import SwiftUI

struct PageOne: View {
    
    @EnvironmentObject private var coordinator: Coordinator
    
    var body: some View {
        ScrollView {
            VStack(spacing: 50) {
                Image(systemName: "house").padding(.top, 200)
                    .font(.largeTitle)
                Text("This the content for the first page")
                HStack{
                    Button("Page Two"){
                        coordinator.push(.pageTwo)
                    }
                    Image(systemName: "chevron.right")
                }
                .foregroundStyle(.blue)
            }
            .navigationTitle("Page One")
        }
    }
}

#Preview {
    PageOne()
}
