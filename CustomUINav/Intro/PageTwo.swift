//
//  PageTwo.swift
//  CustomUINav
//
//  Created by Curtis Turk on 16/10/2023.
//

import Foundation
import SwiftUI

struct PageTwo: View {
    
    @EnvironmentObject private var coordinator: Coordinator
    
    var body: some View {
        ScrollView{
            VStack(spacing: 50){
                Text("Choose a Journey")
                    .padding(.top, 200)
                Image(systemName: "arrow.triangle.branch")
                    .rotationEffect(Angle(degrees: 180))
                    .font(.title)
                HStack{
                    Spacer()
                    VStack{
                        Image(systemName: "lightbulb")
                        Button("Page Three"){
                            coordinator.push(.pageThree)
                        }
                    }
                    Spacer()
                    VStack{
                        Image(systemName: "snowflake")
                        Button("Page Four"){
                            coordinator.push(.pageFour)
                        }
                    }
                    Spacer()
                }
                .foregroundStyle(.blue)
            }
        }
        .navigationTitle("Page Two")
    }
}

#Preview {
    PageTwo()
}

