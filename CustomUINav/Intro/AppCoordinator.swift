//
//  IntroCoordinator.swift
//  CustomUINav
//
//  Created by Curtis Turk on 18/10/2023.
//

import Foundation
import UIKit
import SwiftUI

class AppCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func eventOccured(context: Context) {
        
    }
    
    func start() {
        let didTapButton: ()-> Void = {
            print("Coordinator action triggered")
        }
        let hc = UIHostingController(rootView: PageOne(didTapButton: didTapButton))
        
//        var vc: UIViewController & Coordinating  = ViewController()
//        vc.coordinator = self
        
        navigationController.setViewControllers([hc], animated: false)
        
    }
}
