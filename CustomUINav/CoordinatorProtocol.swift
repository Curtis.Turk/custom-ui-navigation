//
//  Coordinator.swift
//  CustomUINav
//
//  Created by Curtis Turk on 18/10/2023.
//

import Foundation
import UIKit

enum Page {
    case pageOne
    case pageTwo
    case pageThree
}

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    
    func eventOccured(context: Context)
    
    func start()
}

protocol Coordinating {
    var coordinator: Coordinator? { get set }
}
